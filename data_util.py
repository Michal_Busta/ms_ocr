'''
this file is modified from keras implemention of data process multi-threading,
see https://github.com/fchollet/keras/blob/master/keras/utils/data_utils.py
'''
import time
import numpy as np
import threading
import multiprocessing
import pandas as pd
import math
import cv2

try:
    import queue
except ImportError:
    import Queue as queue


def remove_pairs(pairs, labels, resize):
    w, h = resize, resize
    res_pairs = []
    res_labels = []

    for i in range(len(labels)):
        act_pair = np.hstack(pairs[i])
        if (act_pair < 0).any():
            continue
        # discard if x is greater than width
        if (act_pair[::2] >= w).any():
            continue

        if (act_pair[1::2] >= h).any():
            continue
        # if act_pair

        res_pairs.append(pairs[i])
        res_labels.extend([labels[i]])



    return (np.array(res_pairs)/4).astype(int), res_labels





def test_dists():
    dists_i = pd.read_hdf('/home/klara/klara/home/DeepSemanticText/resources/235000.h5', 'table').as_matrix()
    dists_f = pd.read_hdf('/home/klara/klara/home/DeepSemanticText/resources/235500.h5', 'table').as_matrix()
    pairs = pd.read_pickle('/home/klara/klara/home/Embedding/resources/pairs2.pkl')
    pairs = pairs.loc[pairs['Name'] == 'B006CRDEGW']
    labels = pairs['GT'].as_matrix()
    pairs.as_matrix()


    res = []
    for i in range(len(labels)):
        res.append([labels[i], dists_i[i], dists_f[i]])
    res = pd.DataFrame(res, columns=['Label', 'Initial distance', 'Final distance'])


    print('Same display:')
    print(res.loc[res['Label']==1])
    print('Different display:')
    print(res.loc[res['Label'] == 0])

    dists_i = pd.read_hdf('/home/klara/klara/home/DeepSemanticText/resources/235000.h5', 'table').as_matrix()
    dists_f = pd.read_hdf('/home/klara/klara/home/DeepSemanticText/resources/236000.h5', 'table').as_matrix()

    res = []
    for i in range(len(labels)):
        res.append([labels[i], dists_i[i], dists_f[i]])
    res = pd.DataFrame(res, columns=['Label', 'Initial distance', 'Final distance'])

    print('Same display:')
    print(res.loc[res['Label'] == 1])
    print('Different display:')
    print(res.loc[res['Label'] == 0])


def create_ims_file():
    pairs = pd.read_pickle('/home/klara/klara/home/Embedding/resources/full_gt_pairs.pkl')
    names = pairs['Name'].drop_duplicates().as_matrix()

    with open('/home/klara/klara/home/DeepSemanticText/resources/ims_fullgt.txt', 'a') as f:
        for name in names:
            f.write(name+'\n')




def load_pairs(name, shape, pairs_all):
    h, w = shape
    pairs = pairs_all.loc[pairs_all['Name']==name]
    labels = pairs['GT'].as_matrix()
    pairs = pairs[['C1', 'C2']].as_matrix()
    #w_r =384/w
    #h_r = 896/h
    for i in range(len(pairs)):
        pairs[i, 0]= np.array(pairs[i, 0])
        pairs[i, 1] = np.array(pairs[i, 1])

    for i in range(len(pairs)):
        pairs[i, 0][0], pairs[i, 0][1] = int(pairs[i, 0][0]), int(pairs[i, 0][1])
        pairs[i, 1][0], pairs[i, 1][1] = int(pairs[i, 1][0]), int(pairs[i, 1][1] )
    
    if pairs.shape[0] > 0:
      pairs =np.vstack(np.hstack(pairs)).reshape(len(pairs), 2, 2)
    return pairs, labels


class GeneratorEnqueuer():
    """Builds a queue out of a data generator.

    Used in `fit_generator`, `evaluate_generator`, `predict_generator`.

    # Arguments
        generator: a generator function which endlessly yields data
        use_multiprocessing: use multiprocessing if True, otherwise threading
        wait_time: time to sleep in-between calls to `put()`
        random_seed: Initial seed for workers,
            will be incremented by one for each workers.
    """

    def __init__(self, generator,
                 use_multiprocessing=False,
                 wait_time=0.05,
                 random_seed=None):
        self.wait_time = wait_time
        self._generator = generator
        self._use_multiprocessing = use_multiprocessing
        self._threads = []
        self._stop_event = None
        self.queue = None
        self.random_seed = random_seed

    def start(self, workers=1, max_queue_size=10):
        """Kicks off threads which add data from the generator into the queue.

        # Arguments
            workers: number of worker threads
            max_queue_size: queue size
                (when full, threads could block on `put()`)
        """

        def data_generator_task():
            while not self._stop_event.is_set():
                try:
                    if self._use_multiprocessing or self.queue.qsize() < max_queue_size:
                        generator_output = next(self._generator)
                        self.queue.put(generator_output)
                    else:
                        time.sleep(self.wait_time)
                except Exception:
                    self._stop_event.set()
                    raise

        try:
            if self._use_multiprocessing:
                self.queue = multiprocessing.Queue(maxsize=max_queue_size)
                self._stop_event = multiprocessing.Event()
            else:
                self.queue = queue.Queue()
                self._stop_event = threading.Event()

            for _ in range(workers):
                if self._use_multiprocessing:
                    # Reset random seed else all children processes
                    # share the same seed
                    np.random.seed(self.random_seed)
                    thread = multiprocessing.Process(target=data_generator_task)
                    thread.daemon = True
                    if self.random_seed is not None:
                        self.random_seed += 1
                else:
                    thread = threading.Thread(target=data_generator_task)
                self._threads.append(thread)
                thread.start()
        except:
            self.stop()
            raise

    def is_running(self):
        return self._stop_event is not None and not self._stop_event.is_set()

    def stop(self, timeout=None):
        """Stops running threads and wait for them to exit, if necessary.

        Should be called by the same thread which called `start()`.

        # Arguments
            timeout: maximum time to wait on `thread.join()`.
        """
        if self.is_running():
            self._stop_event.set()

        for thread in self._threads:
            if thread.is_alive():
                if self._use_multiprocessing:
                    thread.terminate()
                else:
                    thread.join(timeout)

        if self._use_multiprocessing:
            if self.queue is not None:
                self.queue.close()

        self._threads = []
        self._stop_event = None
        self.queue = None

    def get(self):
        """Creates a generator to extract data from the queue.

        Skip the data if it is `None`.

        # Returns
            A generator
        """
        while self.is_running():
            if not self.queue.empty():
                inputs = self.queue.get()
                if inputs is not None:
                    yield inputs
            else:
                time.sleep(self.wait_time)

if __name__ == '__main__':
    create_ims_file()