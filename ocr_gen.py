# coding:utf-8
import csv
import cv2
import time
import os
import numpy as np
import random

from data_util import GeneratorEnqueuer

buckets = [54, 80, 124, 182, 272, 410, 614, 922, 1383] 

f = open('codec.txt', 'r')
codec = f.readlines()[0]
codec_rev = {}
index = 4
for i in range(0, len(codec)):
  codec_rev[codec[i]] = index
  index += 1

def get_images(data_path):
  
  base_dir = os.path.dirname(data_path)
  files_out = []
  cnt = 0
  with open(data_path) as f:
    while True:
      line = f.readline()
      if not line: 
        break
      line = line.strip()
      if len(line) == 0:
        continue
      if not line[0] == '/':
        line = '{0}/{1}'.format(base_dir, line)
      files_out.append(line)  
      cnt +=1
      #if cnt > 10:
      #  break
  return files_out



def generator(batch_size=4, train_list='/home/klara/klara/home/DeepSemanticText/resources/ims2.txt', in_train=True, rgb = False):
  image_list = np.array(get_images(train_list))
  print('{} training images in {}'.format(image_list.shape[0], train_list))
  index = np.arange(0, image_list.shape[0])
  
  
  bucket_images = []
  bucket_labels = []
  bucket_label_len = []
  
  for b in range(0, len(buckets)):
    bucket_images.append([])
    bucket_labels.append([])
    bucket_label_len.append([])
  
  while True:
    if in_train:
      np.random.shuffle(index)
  
    for i in index:
      try:
        image_name = image_list[i]
        if random.uniform(0, 100) < 50:
          image_name = image_list[int(random.uniform(0, min(3000000,image_list.shape[0] - 1)))]
        
        #if len(images) == 0:
        #  im_name = '/home/busta/data/Amazon/B000FC0URG.jpg' 
        #im_name = '/home/busta/data/SynthText/Amazon/B009F2PQY4.jpg'
        
        spl = image_name.split(" ")
        image_name = spl[0].strip()
        gt_txt = ''
        if len(spl) > 1:
          gt_txt = ""
          delim = ""
          for k in range(1, len(spl)):
            gt_txt += delim + spl[k]
            delim =" "
          if gt_txt[0] == '"':
              gt_txt = gt_txt[1:-1]
              

        if image_name[len(image_name) - 1] == ',':
          image_name = image_name[0:-1]    
        
        if not os.path.exists(image_name):
          continue
        
        if rgb:
          im = cv2.imread(image_name)
        else:
          im = cv2.imread(image_name, cv2.IMREAD_GRAYSCALE)
        if im is None:
          continue
        
        scale = 32 / float(im.shape[0])
        width = int(im.shape[1] * scale)
      
        best_diff = width
        bestb = 0
        for b in range(0, len(buckets)):
          if best_diff > abs(width * 1.3 - buckets[b]):
            best_diff = abs(width * 1.3 - buckets[b])
            bestb = b
              
        width =  buckets[bestb]       
        im = cv2.resize(im, (buckets[bestb], 32))
        if not rgb:
          im = im.reshape(im.shape[0],im.shape[1], 1) 
        
        if in_train:
          if random.randint(0, 100) < 10:
            im = np.invert(im)
              
        
        bucket_images[bestb].append(im[:, :, ::-1].astype(np.float32))
        
        gt_labels = []
        for k in range(len(gt_txt)):
          if gt_txt[k] in codec_rev:                
            gt_labels.append( codec_rev[gt_txt[k]] )
          else:
            gt_labels.append( 3 )
        
    
        bucket_labels[bestb].extend(gt_labels)
        bucket_label_len[bestb].append(len(gt_labels))
        
        if len(bucket_images[bestb]) == batch_size:
          images = np.asarray(bucket_images[bestb], dtype=np.float)
          images /= 128
          images -= 1

          yield images, bucket_labels[bestb], bucket_label_len[bestb]
          bucket_images[bestb] = []
          bucket_labels[bestb] = []
          bucket_label_len[bestb]  = []
          
      except Exception as e:
        import traceback
        traceback.print_exc()
        continue
      
    if not in_train:
      print("finish")
      yield None
      break    


def get_batch(num_workers, **kwargs):
  try:
    enqueuer = GeneratorEnqueuer(generator(**kwargs), use_multiprocessing=True)
    enqueuer.start(max_queue_size=24, workers=num_workers)
    generator_output = None
    while True:
      while enqueuer.is_running():
        if not enqueuer.queue.empty():
          generator_output = enqueuer.queue.get()
          break
        else:
          time.sleep(0.01)
      yield generator_output
      generator_output = None
  finally:
    if enqueuer is not None:
      enqueuer.stop()

if __name__ == '__main__':
  
  data_generator = get_batch(num_workers=1, batch_size=1)
  while True:
    data = next(data_generator)
  